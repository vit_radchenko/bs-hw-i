package com.binary_studio.uniq_in_sorted_stream;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(distinctByProperty(Row::getPrimaryId));
	}

	private static <T> Predicate<T> distinctByProperty(Function<? super T, ?> propertyExtractor) {
		Set<Object> set = ConcurrentHashMap.newKeySet();
		return t -> set.add(propertyExtractor.apply(t));
	}

}
