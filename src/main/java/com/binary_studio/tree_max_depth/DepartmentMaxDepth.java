package com.binary_studio.tree_max_depth;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		int depth = 0;

		if (rootDepartment != null) {
			depth++;
			List<Department> childs = collectChilds(Collections.singletonList(rootDepartment));

			while (!childs.isEmpty()) {
				depth++;
				childs = collectChilds(childs);
			}
		}

		return depth;
	}

	private static List<Department> collectChilds(List<Department> childsDepartment) {
		return childsDepartment.stream().flatMap(child -> child.subDepartments.stream()).filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

}
