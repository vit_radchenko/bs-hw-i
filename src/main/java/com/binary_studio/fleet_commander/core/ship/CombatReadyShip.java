package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip ship;

	private final int maxShieldHP;

	private final int maxHullHP;

	private final int maxCapacitorAmount;

	CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.maxCapacitorAmount = this.ship.capacitorAmount.value();
		this.maxHullHP = this.ship.hullHP.value();
		this.maxShieldHP = this.ship.shieldHP.value();
	}

	@Override
	public void endTurn() {
		this.ship.capacitorAmount = PositiveInteger
				.of(this.ship.capacitorAmount.value() + this.ship.capacitorRechargeRate.value());
		if (this.ship.capacitorAmount.value() > this.maxCapacitorAmount) {
			this.ship.capacitorAmount = PositiveInteger.of(this.maxCapacitorAmount);
		}
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.ship.capacitorAmount.value() < this.ship.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		this.ship.capacitorAmount = PositiveInteger
				.of(this.ship.capacitorAmount.value() - this.ship.attackSubsystem.getCapacitorConsumption().value());

		var damage = this.ship.attackSubsystem.attack(target);
		AttackAction attackAction = new AttackAction(damage, this, target, this.ship.attackSubsystem);
		return Optional.of(attackAction);
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		var reducedAttackDamage = this.ship.defenciveSubsystem.reduceDamage(attack);
		var leftDamageAfterShieldReducing = this.ship.shieldHP.value() - reducedAttackDamage.damage.value();

		try {
			this.ship.shieldHP = PositiveInteger.of(this.ship.shieldHP.value() - reducedAttackDamage.damage.value());
		}
		catch (IllegalArgumentException e) {
			this.ship.shieldHP = PositiveInteger.of(0);
		}

		try {
			if (leftDamageAfterShieldReducing < 0) {
				this.ship.hullHP = PositiveInteger.of(this.ship.hullHP.value() + leftDamageAfterShieldReducing);
			}
		}
		catch (IllegalArgumentException e) {
			return new AttackResult.Destroyed();
		}

		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(reducedAttackDamage.damage.value()),
				this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.ship.defenciveSubsystem.getCapacitorConsumption().value() > this.ship.capacitorAmount.value()) {
			return Optional.empty();
		}

		RegenerateAction regeneration = this.ship.defenciveSubsystem.regenerate();
		var hullRegenerated = calculateHullRegeneration(regeneration);
		var shieldRegenerated = calculateShieldRegeneration(regeneration);

		this.ship.capacitorAmount = PositiveInteger
				.of(this.ship.capacitorAmount.value() - this.ship.defenciveSubsystem.getCapacitorConsumption().value());
		this.ship.hullHP = PositiveInteger.of(this.ship.hullHP.value() + hullRegenerated.value());
		this.ship.shieldHP = PositiveInteger.of(this.ship.shieldHP.value() + shieldRegenerated.value());

		return Optional.of(new RegenerateAction(shieldRegenerated, hullRegenerated));
	}

	private PositiveInteger calculateShieldRegeneration(RegenerateAction regeneration) {
		int shieldRegenerated = 0;

		if (this.ship.shieldHP.value() != this.maxShieldHP) {
			if (this.ship.shieldHP.value() + regeneration.shieldHPRegenerated.value() > this.maxShieldHP) {
				shieldRegenerated = regeneration.shieldHPRegenerated.value()
						+ (this.ship.shieldHP.value() + regeneration.shieldHPRegenerated.value()) - this.maxShieldHP;
			}
			else {
				shieldRegenerated = regeneration.shieldHPRegenerated.value();
			}
		}

		return PositiveInteger.of(shieldRegenerated);
	}

	private PositiveInteger calculateHullRegeneration(RegenerateAction regeneration) {
		int hullRegenerated = 0;

		if (this.ship.hullHP.value() != this.maxHullHP) {
			if (this.ship.hullHP.value() + regeneration.hullHPRegenerated.value() > this.maxHullHP) {
				hullRegenerated = regeneration.hullHPRegenerated.value()
						+ (this.ship.hullHP.value() + regeneration.hullHPRegenerated.value()) - this.maxHullHP;
			}
			else {
				hullRegenerated = regeneration.hullHPRegenerated.value();
			}
		}

		return PositiveInteger.of(hullRegenerated);
	}

}
