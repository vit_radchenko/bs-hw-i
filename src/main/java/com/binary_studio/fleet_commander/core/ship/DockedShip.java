package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	String name;

	PositiveInteger shieldHP;

	PositiveInteger hullHP;

	private Integer powergridOutput;

	PositiveInteger capacitorAmount;

	PositiveInteger capacitorRechargeRate;

	PositiveInteger speed;

	PositiveInteger size;

	AttackSubsystem attackSubsystem;

	DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		DockedShip dockedShip = new DockedShip();
		dockedShip.name = name;
		dockedShip.shieldHP = shieldHP;
		dockedShip.hullHP = hullHP;
		dockedShip.powergridOutput = powergridOutput.value();
		dockedShip.capacitorAmount = capacitorAmount;
		dockedShip.capacitorRechargeRate = capacitorRechargeRate;
		dockedShip.speed = speed;
		dockedShip.size = size;

		return dockedShip;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.powergridOutput = this.powergridOutput + this.attackSubsystem.getPowerGridConsumption().value();
		}
		else {
			validatePowerGridCapacity(subsystem);
			this.powergridOutput = this.powergridOutput - subsystem.getPowerGridConsumption().value();
		}

		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.powergridOutput = this.powergridOutput + this.defenciveSubsystem.getPowerGridConsumption().value();
		}
		else {
			validatePowerGridCapacity(subsystem);
			this.powergridOutput = this.powergridOutput - subsystem.getPowerGridConsumption().value();
		}

		this.defenciveSubsystem = subsystem;
	}

	private void validatePowerGridCapacity(Subsystem subsystem) throws InsufficientPowergridException {
		int diff = this.powergridOutput - subsystem.getPowerGridConsumption().value();

		if (diff < 0) {
			throw new InsufficientPowergridException(diff * (-1));
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}

}
