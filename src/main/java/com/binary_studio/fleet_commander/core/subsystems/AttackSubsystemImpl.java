package com.binary_studio.fleet_commander.core.subsystems;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private Integer optimalSpeed;

	private Integer optimalSize;

	private Integer baseDamage;

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
		attackSubsystem.name = name;
		attackSubsystem.powergridRequirments = powergridRequirments;
		attackSubsystem.capacitorConsumption = capacitorConsumption;
		attackSubsystem.optimalSpeed = optimalSpeed.value();
		attackSubsystem.optimalSize = optimalSize.value();
		attackSubsystem.baseDamage = baseDamage.value();

		return attackSubsystem;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = target.getSize().value() >= this.optimalSize ? 1
				: target.getSize().value().doubleValue() / this.optimalSize;
		double speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed ? 1
				: this.optimalSpeed.doubleValue() / (2 * target.getCurrentSpeed().value());

		int symbolAfterDot = 2;
		double damage = this.baseDamage * round(min(sizeReductionModifier, speedReductionModifier), symbolAfterDot);
		return PositiveInteger.of((int) damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

	private Double min(Double a, Double b) {
		if (a < b) {
			return a;
		}
		return b;
	}

	private static double round(double value, int places) {
		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(Double.toString(value));
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
